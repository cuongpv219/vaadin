$(document).ready(function() {
	$('#myTable').DataTable( {
		scrollY:        500,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
		"order": [[ 0, "desc" ]],
	    fixedColumns:   {
            leftColumns: 1
        },
	} );
})